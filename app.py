from consoles import UkraineConsole, PolandConsole
from models import *


def main():
    # console = UkraineConsole(UkraineRada, UkraineFraction, UkraineDeputat)
    console = PolandConsole(PolandRada, PolandFraction, PolandDeputat)
    console.run()


if __name__ == '__main__':
    main()
